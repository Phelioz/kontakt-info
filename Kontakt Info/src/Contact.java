
public class Contact {

	String fNamn;
	String eNamn;
	String nummer;
	
	
	public Contact(String fNamn, String eNamn, String nummer){
		this.fNamn = fNamn;
		this.eNamn = eNamn;
		this.nummer = nummer;
	}
	
	public Contact(){
		this.fNamn = "";
		this.eNamn = "";
		this.nummer = "";
	}


	public String getfNamn() {
		return fNamn;
	}


	public void setfNamn(String fNamn) {
		this.fNamn = fNamn;
	}


	public String geteNamn() {
		return eNamn;
	}


	public void seteNamn(String eNamn) {
		this.eNamn = eNamn;
	}


	public String getNummer() {
		return nummer;
	}


	public void setNummer(String inputString) {
		this.nummer = inputString;
	}

	@Override
	public String toString() {
		return "F�rnamn: " + fNamn + "\n | Efternamn: " + eNamn + "\n | Nummer: "
				+ nummer;
	}
	
	
	/*
	public Contact(){
		fNamn = new ArrayList<String>();
		eNamn = new ArrayList<String>();
		nummer = new ArrayList<Integer>();
	}
	
	public void add(String fNamn, String eNamn, int nummer){
		this.fNamn.add(fNamn);
		this.eNamn.add(eNamn);
		this.nummer.add(nummer);
	}
	
	public ArrayList<String> getfNamn() {
		return fNamn;
	}

	public void setfNamn(ArrayList<String> fNamn) {
		this.fNamn = fNamn;
	}

	public ArrayList<String> geteNamn() {
		return eNamn;
	}

	public void seteNamn(ArrayList<String> eNamn) {
		this.eNamn = eNamn;
	}

	public ArrayList<Integer> getNummer() {
		return nummer;
	}

	public void setNummer(ArrayList<Integer> nummer) {
		this.nummer = nummer;
	}
	*/
}

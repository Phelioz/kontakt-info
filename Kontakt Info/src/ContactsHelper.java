import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class ContactsHelper {

	Scanner inputScanner = new Scanner(System.in);
	PrintWriter outPrintWriter;
	String inputString;

	boolean first = true;
	boolean stop = false;
	
	ArrayList<Contact> contacts;
	
	public ContactsHelper(){
	
	}
	
	public ArrayList<Contact> createContacts(){
		contacts = new ArrayList<Contact>();
		outerloop:
		while(true){
			Contact tempContact = new Contact();
			if(first == false){
				System.out.print("Vill du l�gga till en till kontakt? Skriv ja eller nej: ");
				inputString = inputScanner.next();
				if(inputString.equalsIgnoreCase("nej")){ break outerloop;
				}
			}
			System.out.print("Skriv f�rnamn: ");
			inputString = inputScanner.next();
			tempContact.setfNamn(inputString);
			
			System.out.print("Skriv efternamn: ");
			inputString = inputScanner.next();
			tempContact.seteNamn(inputString);
			
			while (true) {
				System.out.print("Skriv nummer: ");
				inputString = inputScanner.next();
				if(isInteger(inputString) == true) break;
			}
			tempContact.setNummer(inputString);

			contacts.add(tempContact);

			if(first == true) first = false;
			}
		return contacts;
	}

	private boolean isInteger(String inputString) {
		boolean isNumeric = true;
		char charArr[] = inputString.toCharArray();
		for (char c : charArr) {
			if (!Character.isDigit(c)) {
				isNumeric = false;
			}
		}
		return isNumeric;
	}
	public void printContacts(ArrayList<Contact> contacts){
		for (int i = 0; i < contacts.size(); i++) {
			System.out.println("\n" + contacts.get(i));
			if (i == contacts.size() - 1) {
				System.out.println("");
			}
		}
	}

	public void writeContactsToFile(ArrayList<Contact> contacts) throws FileNotFoundException{
		System.out.print("Namnet p� filen d�r alla kontakter ska ligga: ");
		inputString = inputScanner.next();
		File file = new File("kontakter/" + inputString + ".txt");
		outPrintWriter = new PrintWriter(file);
		
		for (int i = 0; i < contacts.size(); i++) {
			outPrintWriter.println(contacts.get(i));
		}
		outPrintWriter.flush();
		
		
	}

	public void readContact() {
		Scanner contactScanner; 
		
		while (true) {
			System.out
					.print("Skriv namnet p� kontakt filen du vill l�sa upp: ");
			inputString = ("kontakter/" + inputScanner.next()) + ".txt";
			File contactFile = new File(inputString);
			if (contactFile.exists()) {

				try {
					contactScanner = new Scanner(contactFile);
					System.out.println("");
					while (contactScanner.hasNext()) {
						boolean last = false;
						if(contactScanner.hasNextInt()) last = true;
				
						System.out.println(contactScanner.next());
				
						if (last == true) {
							System.out.println("");
							last = false;
						}
					}
					break;

				} catch (FileNotFoundException e) {
					System.out.println("Filen hittades inte");
				}
				
			}
		}
	}
}

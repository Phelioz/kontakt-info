import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	static ArrayList<Contact> contacts;
	static ContactsHelper contactsMaker;
	static Scanner inputScanner;
	static String inputString;

	public static void main(String[] args) {
		contactsMaker = new ContactsHelper();
		inputScanner = new Scanner(System.in);
		while (true) {
			System.out
					.println("\nSkriv l�s f�r att l�sa kontakter | Skriv f�r att skapa nya | Skriv sluta f�r att avsluta");
			inputString = inputScanner.next();

			if (inputString.equalsIgnoreCase("sluta"))
				break;

			if (inputString.equalsIgnoreCase("skapa")) {
				contacts = contactsMaker.createContacts();
				contactsMaker.printContacts(contacts);
				try {
					contactsMaker.writeContactsToFile(contacts);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
			if (inputString.equalsIgnoreCase("l�s")) {
						contactsMaker.readContact();
					
				
			}
		}
	}

}
